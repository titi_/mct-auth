const express = require('express');
const {google} = require('googleapis');
const {Client} = require('pg');

const app = express();

app.get('/get-token/:domaine', async (req, res) => {
    const domaine = req.params.domaine;
    console.log(domaine)
    const connectionString = process.env.DATABASE_URL;

    const client = new Client({
        connectionString,
        ssl: {
            rejectUnauthorized: false
        }
    });

    try {
        await client.connect();

        const query_results = await client.query("SELECT * FROM ade_admin_emails WHERE admin_email LIKE '%' || $1 || '%'", [domaine]);

        // if no admin email is found for the given domain
        if (query_results.rows.length === 0) {

            // add the headers to the response
            res.header("Access-Control-Allow-Origin", `chrome-extension://${process.env.EXTENSION_ID}`);
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

            res.json({
                error: "no_admin_email"
            });
            return;
        }

        const admin_email = query_results.rows[0].admin_email;
        const serviceAccountKey = JSON.parse(process.env.GOOGLE_APPLICATION_CREDENTIALS);

        const jwtClient = new google.auth.JWT({
            email: serviceAccountKey.client_email,
            key: serviceAccountKey.private_key,
            scopes: ['https://www.googleapis.com/auth/admin.directory.user'],
            subject: admin_email
        });

        const token = await jwtClient.getAccessToken();

        res.header("Access-Control-Allow-Origin", `chrome-extension://${process.env.EXTENSION_ID}`);
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        res.json({
            token: token.token
        });
    } catch (error) {
        console.error(error);
    } finally {

        await client.end();
    }
});


app.listen(process.env.PORT || 10000, () => {
    console.log(`Server is running on port ${process.env.PORT}`);
});
